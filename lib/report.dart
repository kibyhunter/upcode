import 'package:flutter/material.dart';

class Report extends StatefulWidget {
  const Report({Key? key}) : super(key: key);

  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<Report> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Report', style: TextStyle(fontSize: 40))),
      body: Center(
        child: Text('Report Screen', style: TextStyle(fontSize: 40)),
      )
    );
  }
}
