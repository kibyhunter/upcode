import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height:50),
            Container(
              width: double.infinity,
              height: 300,
              color: Color(0xFF031476),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Container(
                  child: Column (
                    children: <Widget>[

// Hộp 1
                      Container(
                        height: 90,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment. start,
                          children: [
                            Text('YOUR ENGLISHSCORE',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.pinkAccent
                              )
                            ),

                            SizedBox(height: 7),
                              Text('A2 • Elementary',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight:FontWeight.bold,
                                )
                              ),

                            SizedBox(height: 10),
                            LinearPercentIndicator(
                              width: 250,
                              percent: 60/100,
                              animation: true,
                              progressColor: Colors.red,
                            ) ,
                          ],
                        ),
                      ),

//Hộp 2

                      Container(
                        height: 45,
                        child: Row(
                          children: [
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment. start,

                                children: [
                                  Text('Grammar • 294',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,

                                      )
                                  ),

                                  SizedBox(height: 8),
                                  LinearPercentIndicator(
                                    width: 120,
                                    percent: 35/100,
                                    animation: true,
                                    progressColor: Colors.red,
                                  ),
                                ],
                              )
                            ),

                            Container(
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment. start,

                                    children: [
                                      Text('Reading • 294',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.white,
                                          )
                                      ),

                                      SizedBox(height: 8),
                                      LinearPercentIndicator(
                                        width: 120,
                                        percent: 35/100,
                                        animation: true,
                                        progressColor: Colors.red,
                                      ) ,

                                    ],
                                  ),
                                )
                            )

                          ],
                        )
                      ),

// Hộp 3

                      SizedBox(height: 20),

                      Container(
                          height: 45,

                          child: Row(
                            children: [
                              Container(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment. start,

                                    children: [
                                      Text('Vocabulary • 294',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.white,

                                          )
                                      ),

                                      SizedBox(height: 8),
                                      LinearPercentIndicator(
                                        width: 120,
                                        percent: 30/100,
                                        animation: true,
                                        progressColor: Colors.red,
                                      ),
                                    ],
                                  )
                              ),

                              Container(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment. start,

                                      children: [
                                        Text('Listening • 294',
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.white,
                                            )
                                        ),

                                        SizedBox(height: 8),
                                        LinearPercentIndicator(
                                          width: 120,
                                          percent: 35/100,
                                          animation: true,
                                          progressColor: Colors.red,
                                        ) ,

                                      ],
                                    ),
                                  )
                              )

                            ],
                          )
                      ),



                      SizedBox(height:20),
// Hộp 4
                      Container(
                        child: Row(
                          children: [
                            Container(
                              alignment: AlignmentDirectional.center,
                              width: 90,
                              height: 35,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                color: Colors.blue,
                              ),
                              child: Text("CRFR A2",
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,

                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                              child: Container(
                                alignment: AlignmentDirectional.center,
                                width: 90,
                                height: 35,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                  color: Colors.blue,
                                ),
                                child: Text("IELTS 3.5",
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,

                                  ),
                                ),
                              ),
                            )

                          ],
                        ),
                      )



                    //***************

                    ],
                  )
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

