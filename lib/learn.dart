import 'package:flutter/material.dart';

class Learn extends StatefulWidget {
  const Learn({Key? key}) : super(key: key);

  @override
  _LearnState createState() => _LearnState();
}

class _LearnState extends State<Learn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Learn', style: TextStyle(fontSize: 40))),
        body: Center(
          child: Text('Learn Screen', style: TextStyle(fontSize: 25)),
        )
    );
  }
}
