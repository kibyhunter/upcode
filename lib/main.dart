import 'package:flutter/material.dart';
import 'package:pr1/home.dart';
import 'package:pr1/learn.dart';
import 'package:pr1/report.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}



class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentTab =0;
  final List<Widget> screen = [
    Home(),
    Report(),
    Learn(),
  ];
  final PageStorageBucket bucket = PageStorageBucket(); // hiển thị những tab
  Widget currentScreen = Home();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          color: Color(0xFF031476),
          height: 60,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MaterialButton(
                  minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Home();
                        currentTab=0;
                      });
                    },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.home,
                          color: currentTab == 0 ? Colors.red : Colors.white,
                          size: 30,
                      ),
                      Text('Home', style: TextStyle(
                          fontSize: 8,
                          color: currentTab == 0 ? Colors.red : Colors.white)
                      )
                    ],
                  )
                ),

                MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Report();
                        currentTab=1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.report,
                          color: currentTab == 1 ? Colors.red : Colors.white,
                          size: 30,
                        ),
                        Text('Report', style: TextStyle(
                            fontSize: 8,
                            color: currentTab == 1 ? Colors.red : Colors.white)
                        )
                      ],
                    )
                ),

                MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = Learn();
                        currentTab= 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.menu_book_sharp,
                          color: currentTab == 2 ? Colors.red : Colors.white,
                          size: 30,
                        ),
                        Text('Learn', style: TextStyle(
                            fontSize: 8,
                            color: currentTab == 2 ? Colors.red : Colors.white)
                        )
                      ],
                    )
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}



